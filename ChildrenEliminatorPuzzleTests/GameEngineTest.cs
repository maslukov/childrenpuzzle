﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChildrenEliminatorPuzzle;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChildrenEliminatorPuzzleTests
{
    [TestClass]
    public class GameEngineTest
    {
        private EliminationGame _engine;

        [TestInitialize]
        public void RunBeforeEachTest()
        {
            _engine = new EliminationGame(new LinkedListStorage());
        }

        [TestMethod]
        public void SinglePlayer_ShouldWin()
        {
            var result = _engine.Run(new GameParameters(1, 1));
            AssertWinningPlayer(result,1);
            Assert.IsFalse(result.Eliminated.Any());
        }

        [TestMethod]
        public void TwoPlayers_WinsFirstIf_EliminationCycleIsEvent()
        {
            var EVEN_NUMBER = 6;
            var result = _engine.Run(new GameParameters(2, EVEN_NUMBER));

            AssertWinningPlayer(result, 1);
            AssertSequence(result.Eliminated, 2);
        }

        [TestMethod]
        public void TwoPlayers_WinsLast_EliminationCycleIsOdd()
        {
            var ODD_NUMBER = 3;
            var result = _engine.Run(new GameParameters(2, ODD_NUMBER));
            AssertWinningPlayer(result, 2);
            AssertSequence(result.Eliminated, 1);
        }

        [TestMethod]
        public void ThreePlayers_EliminatedInARow()
        {
            var result = _engine.Run(new GameParameters(3, 1));
            Assert.AreEqual(3, result.WinningPlayer);
            AssertSequence(result.Eliminated, 1, 2);
        }

        [TestMethod]
        public void TenPlayers_ShouldBeEliminatedAsPerRules()
        {
            var engine = new EliminationGame(new LinkedListStorage());
            var result = engine.Run(new GameParameters(10, 5));
            AssertWinningPlayer(result, 3);
            AssertSequence(result.Eliminated, 5, 10, 6, 2, 9, 8, 1, 4, 7);
        }

        [TestMethod]
        [ExpectedException(typeof (Exception))]
        public void Fails_IfNumberOfChildren_NotPositive()
        {
            _engine.Run(new GameParameters(0, 1));
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentNullException))]
        public void Fails_IfNullGameParameter()
        {
            _engine.Run(null);
        }

        [TestMethod]
        [ExpectedException(typeof (Exception))]
        public void Fails_IfEliminationCycle_NotBePositive()
        {
            _engine.Run(new GameParameters(100, 0));
        }

        /// <summary>
        /// Helper method to compare winniong player and genearte meaningful error
        /// </summary>
        /// <param name="result">game result</param>
        /// <param name="expectedWinningPlayer">expected winning player</param>
        private void AssertWinningPlayer(IGameResults result, int expectedWinningPlayer)
        {
            Assert.IsNotNull(result, "Game Result should not be NULL");
            Assert.AreEqual(expectedWinningPlayer, result.WinningPlayer, "Winning player is not matching");
        }

        /// <summary>
        /// Helper Assert method to compare two sequences and generate meaningful error when they are not matching
        /// </summary>
        /// <param name="actualSequence">actual sequence</param>
        /// <param name="expectedSequence">expected sequence</param>
        private void AssertSequence(IEnumerable<int> actualSequence, params int[] expectedSequence)
        {
            var actualList = actualSequence.ToList();

            // verify sequence Length
            Assert.AreEqual(expectedSequence.Length, actualList.Count,
                string.Format("Expected {0} element, actual {1}. Sequence is:{2}",
                    expectedSequence.Length, actualList.Count,string.Join(",", actualList)));

            // verify sequence content
            for (int index = 0; index < actualList.Count; index++)
            {
                Assert.AreEqual(expectedSequence[index], actualList[index],
                    string.Format("Element No {0} is not matching. ", index + 1));
            }
        }
    }
}
