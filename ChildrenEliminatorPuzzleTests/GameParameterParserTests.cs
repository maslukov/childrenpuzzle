﻿using System;
using ChildrenEliminatorPuzzle;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChildrenEliminatorPuzzleTests
{
    [TestClass]
    public class GameParameterParserTests
    {
        private GameParametersParser _parser;

        [TestInitialize]
        public void RunBeforeEachTest()
        {
            _parser = new GameParametersParser();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ShouldFail_IfNullIsPassed()
        {
            _parser.Parse(null);
        }

        [TestMethod]    
        [ExpectedException(typeof(Exception))]
        public void ShouldFail_IfNoParametersArePassed()
        {
            _parser.Parse(new string[] { });
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ShouldFail_IfMoreThanTwoParameters()
        {
            _parser.Parse(new[] { "1", "2", "3" });
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ShouldFail_IfParametersAreNotIntegers()
        {
            _parser.Parse(new[] { "abc", "2" });
        }

        [TestMethod]    
        public void ShouldParse_ValidParameters()
        {
            var gamePar = _parser.Parse(new[] { "10", "2" });
            Assert.IsNotNull(gamePar);
            Assert.AreEqual(10,gamePar.NumberOfPlayers);
            Assert.AreEqual(2, gamePar.EliminationCount);
        }
    }
}
