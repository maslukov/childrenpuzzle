﻿using System;
using ChildrenEliminatorPuzzle;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChildrenEliminatorPuzzleTests
{
    /// <summary>
    /// the following tests are created to demonsrate performance of LinkedList vs BitArray storage on large numbers
    /// </summary>
    [TestClass]
    public class PerformanceTest
    {
        [TestMethod]
        public void Performance_LinkedListSpeed()
        {
            var engine = new EliminationGame(new LinkedListStorage());
            engine.Run(new GameParameters(1000000, 100));  
        }

        [TestMethod]
        public void Performance_BitArraySpeed()
        {
            var engine = new EliminationGame(new BitArrayStorage());
            engine.Run(new GameParameters(1000000, 100));       
        }
    }
}
