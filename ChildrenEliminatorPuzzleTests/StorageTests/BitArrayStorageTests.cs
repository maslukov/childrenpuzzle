﻿using System;
using ChildrenEliminatorPuzzle;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChildrenEliminatorPuzzleTests.StorageTests
{
    /// <summary>
    ///  all tests are specified in base class and will be execuetd agains BitArray
    /// </summary>
    [TestClass]
    public class BitArrayStorageTests : BaseStorageUnitTest
    {
        [TestInitialize]
        public void RunBeforeEachTest()
        {
            _storage = new BitArrayStorage();
        }

    }
}
