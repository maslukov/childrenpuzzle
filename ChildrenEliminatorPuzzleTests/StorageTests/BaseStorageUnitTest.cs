﻿using System;
using ChildrenEliminatorPuzzle;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChildrenEliminatorPuzzleTests.StorageTests
{
    /// <summary>
    /// Base class is used to test any storage that implements ICircularStorage interface
    /// </summary>
    public class BaseStorageUnitTest
    {
        protected ICircularStorage _storage;

        [TestMethod]
        public void AllocatingPlayers_ShouldPosition_OnFirstElement()
        {
            _storage.Allocate(10);
            Assert.AreEqual(10, _storage.Count);
            Assert.AreEqual(1, _storage.Current);
        }

        [TestMethod]
        public void Moving_ShouldMoveToNextElement()
        {
            _storage.Allocate(10);
            _storage.Move(1);
            Assert.AreEqual(2, _storage.Current);
        }

        [TestMethod]
        public void Moving_ShouldBeCircular()
        {
            _storage.Allocate(10);
            _storage.Move(10);
            Assert.AreEqual(1, _storage.Current);
        }


        [TestMethod]
        public void RemovingCurrent_ShouldMovePositionForward()
        {
            _storage.Allocate(10);

            var removed = _storage.RemoveCurrent();

            Assert.AreEqual(1, removed);
            Assert.AreEqual(2, _storage.Current);
            Assert.AreEqual(9, _storage.Count);
        }

        [TestMethod]
        public void Moving_Removing_MultipleTimes()
        {
            _storage.Allocate(10);

            _storage.Move(4);
            int removedFirst = _storage.RemoveCurrent();
            Assert.AreEqual(5, removedFirst);

            _storage.Move(4);
            int removedSecond = _storage.RemoveCurrent();
            Assert.AreEqual(10, removedSecond);

            int removedThird = _storage.RemoveCurrent();
            Assert.AreEqual(1, removedThird);

            Assert.AreEqual(7, _storage.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Fails_IfNumberOfPlayers_Zero()
        {
            _storage.Allocate(0);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Fails_IfUsingWithoutAllocation()
        {
            _storage.Move(1);
        }
     
    }
}
