﻿namespace ChildrenEliminatorPuzzle
{
    /// <summary>
    /// Circular storage
    /// </summary>
    public interface ICircularStorage
    {
        /// <summary>
        /// Allocate storage
        /// </summary>
        /// <param name="numberOfElements"></param>
        void Allocate(int numberOfElements); 

        /// <summary>
        /// returns number of elements
        /// </summary>
        int Count { get;  }

        /// <summary>
        /// returns value of current element
        /// </summary>
        int Current { get; }

        /// <summary>
        /// move from current element forward 
        /// </summary>
        /// <param name="numberOfSteps">number of steps</param>
        void Move(int numberOfSteps);

        /// <summary>
        /// Remove current element
        /// </summary>
        /// <returns>removed element value</returns>
        int RemoveCurrent();
    }
   
}