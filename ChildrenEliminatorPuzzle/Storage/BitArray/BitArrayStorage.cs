﻿using System;
using System.Collections;

namespace ChildrenEliminatorPuzzle
{
    /// <summary>
    /// The following storage was created as alternative to LinkedList. It provides much better memory footprint but slower
    /// Pros:
    ///   memory usage. it is 160 (!) times more efficient than LinkedList . it uses only 1 bit per element
    /// Cons:
    ///   Speed. Advancing to the next element is O(N*K). Total complexity will be O(N^2*K)
    /// </summary>
    public class BitArrayStorage : ICircularStorage
    {
        private BitArray _bitArray;
        private int _numberOfNonZeroElements;
        private int _currentIndex;

        public void Allocate(int numberOfElements)
        {
            if (numberOfElements == 0)
            {
                throw new Exception("Number of elements must not be zero");
            }

            _bitArray = new BitArray(numberOfElements, true);
            _numberOfNonZeroElements = numberOfElements;
            _currentIndex = 0;
        }

        private void VerifyAllocation()
        {
            if (_bitArray == null)
                throw new Exception("Canot be used without Allocation");
        }

        public int Count
        {
            get
            {
                return _numberOfNonZeroElements;
            }
        }

        public int Current
        {
            get { return _currentIndex + 1; }
        }

        public void Move(int numberOfSteps)
        {
            VerifyAllocation();

            if (numberOfSteps < 0)
            {
                throw new Exception("Number of steps should not be negative");
            }

            // optimise the movement by using the fact that iteration is cyclic ( hence use MOD )
            int requiredNumberOfSteps = numberOfSteps % _numberOfNonZeroElements;


            for (int i = 0; i < requiredNumberOfSteps; i++)
            {
                Advance();
            }
        }

        private void Advance()
        {
            do
            {
                _currentIndex++;
                if (_currentIndex > _bitArray.Length - 1)
                {
                    _currentIndex = 0;
                }
            } while (_bitArray[_currentIndex] == false);

        }

        public int RemoveCurrent()
        {
            VerifyAllocation();

            var removedValue = Current;
            if (_currentIndex >= _bitArray.Length)
            {
                throw new Exception(
                    string.Format("Unable to remove item No {0}. Storage size is:{1}", _currentIndex, _bitArray.Length));
            }

            _bitArray[_currentIndex] = false;
            _numberOfNonZeroElements--;

            if (Count>0)
                Advance();

            return removedValue;
        }

  
    }
}
