﻿using System;
using System.Collections.Generic;

namespace ChildrenEliminatorPuzzle
{
    /// <summary>
    /// this is the easiest to perceive and support implementation. It also gives a great speed
    /// Pros:
    ///   Speed 
    ///     Removing element is O{1}
    ///     Advancing to next element is O(K). total complexity O(N^K)
    /// Cons:
    ///   memory consumption (=N*20 bytes = 160bit)
    ///     2 pointers (64 bit each) + 1 integer per element = 20 bytes per element.
    ///   
    /// </summary>
    public class LinkedListStorage : ICircularStorage
    {
        private LinkedList<int> _linkedList;
        private LinkedListNode<int> _currentItem;       

        public void Allocate(int numberOfElements)
        {
            if (numberOfElements <= 0)
            {
                throw new Exception("Number of elements must be positive");
            }

            _linkedList = new LinkedList<int>();
            
            for (var i = 1; i <= numberOfElements; i++)
            {
                _linkedList.AddLast(i);
            }
            _currentItem = _linkedList.First;
        }

        private void VerifyAllocation()
        {
            if(_linkedList==null)
                throw new Exception("Canot be used without Allocation");
        }

        public int RemoveCurrent()
        {
            VerifyAllocation();

            if (_currentItem == null)
            {
                throw new Exception("Unable to remove item, current position is undefined");
            }

            var removedValue = Current;

            var nextItem = AdvanceForward(_currentItem);

            // remove item using internal iterator reference to current LinkedList Item
            _linkedList.Remove(_currentItem);
          
             _currentItem = Count > 0 ? nextItem : null;

            return removedValue;
        }

        public int Count
        {
            get
            {
                VerifyAllocation();
                return _linkedList.Count;
            }
        }

        public int Current
        {
            get
            {
                VerifyAllocation();

                if(_currentItem==null)
                    throw new Exception("Current position is undefined");

                return _currentItem.Value;
            }
        }

        public void Move(int numberOfSteps)
        {
            VerifyAllocation();

            if (numberOfSteps < 0)
            {
                throw new Exception("Number of steps should not be negative");
            }

            // optimise the movement by using the fact that iteration is cyclic ( hence use MOD )
            int requiredNumberOfSteps = numberOfSteps % _linkedList.Count;

            for (int i = 0; i < requiredNumberOfSteps; i++)
            {
                _currentItem = AdvanceForward(_currentItem);
            }
        }

        private LinkedListNode<int> AdvanceForward(LinkedListNode<int> node)
        {
            // if we reached end of the list - start over from the first element
            var nextElement = node.Next ?? _linkedList.First;
            return nextElement;
        }

    }
}