﻿using System;

namespace ChildrenEliminatorPuzzle
{

    public class GameParametersParser
    { 
        public GameParameters Parse(string[] arguments)
        {
            if (arguments == null)
            {
                throw new ArgumentNullException("arguments","Expected agruments to be parsed");
            }

            if (arguments.Length == 0)
            {
                throw new Exception("Please specify two numbers - Players count (N) and elimination count (K)");
            }

            if (arguments.Length != 2)
            {
                throw new Exception(string.Format("Expected two parameters - Players count (N) and elimination count (K) but received {0} parameters.",arguments.Length));
            }

            int numberOfPlayers = ParseParameter(arguments[0], "Players count(N)");
            int eliminationCount = ParseParameter(arguments[1], "Elimination count (K)");
      
            return new GameParameters(numberOfPlayers,eliminationCount);
                
        }

        private int ParseParameter(string argumentToParse, string parameterName)
        {
            int parameterValue;
            if (!int.TryParse(argumentToParse, out parameterValue))
            {
                throw new Exception(string.Format("Unable to parse {0} ({1}) to an integer.",parameterName, argumentToParse));
            }
            return parameterValue;
        }

    }
}
