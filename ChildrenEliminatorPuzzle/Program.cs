﻿using System;
using System.IO;

namespace ChildrenEliminatorPuzzle
{
    class Program
    {            
        static void Main(string[] arguments)
        {
            try
            {
                // the dependecy injection library should take away all NEW keywords
                var parser = new GameParametersParser();
                var gameParameters = parser.Parse(arguments);
                var game = new EliminationGame(new LinkedListStorage());
                var gameResult =  game.Run(gameParameters);

                PrintResults(gameParameters,gameResult, Console.Out);
            }
            catch (Exception ex)
            {
                ReportError(ex, Console.Out);
            }

            Console.Write("...");
            Console.ReadLine();
        }

        private static void ReportError(Exception ex, TextWriter output)
        {
            output.WriteLine("Error: {0}", ex.Message);
        }

        private static void PrintResults(GameParameters gameParameters, IGameResults result, TextWriter output)
        {
            output.WriteLine("Elimination game results for number of players(N): {0} elimination count(K):{1}", gameParameters.NumberOfPlayers, gameParameters.EliminationCount);

            var eliminatedSequence = string.Join(",", result.Eliminated);

            output.WriteLine("Order of eliminated Players: {0}", eliminatedSequence);
            output.WriteLine("Winning Player: {0}", result.WinningPlayer);
        }

    }
}
