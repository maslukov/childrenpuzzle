﻿namespace ChildrenEliminatorPuzzle
{
    /// <summary>
    /// Holds game parameters - number of players and elimination count (N and K)
    /// </summary>
    public class GameParameters
    {
        private readonly int _numberOfPlayers;
        private readonly int _eliminationCount;

        public GameParameters(int numberOfPlayers, int eliminationCount)
        {
            _numberOfPlayers = numberOfPlayers;
            _eliminationCount = eliminationCount;
        }

        public int NumberOfPlayers
        {
            get { return _numberOfPlayers; }
        }

        public int EliminationCount
        {
            get { return _eliminationCount; }
        }
    }
}