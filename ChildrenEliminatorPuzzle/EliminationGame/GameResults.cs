﻿using System;
using System.Collections.Generic;

namespace ChildrenEliminatorPuzzle
{

    /// <summary>
    /// Game result
    /// </summary>
    public interface IGameResults
    {
        /// <summary>
        /// winnning player
        /// </summary>
        int WinningPlayer { get;  }

        /// <summary>
        /// sequnece of eliminated players
        /// </summary>
        IEnumerable<int> Eliminated { get;  }
    }

    /// <summary>
    /// stores Game results - winning player and eliminated sequence
    /// </summary>
    public class GameResults : IGameResults
    {
        private int _eliminatedIndex;
        private readonly int[] _sequenceOfElimination; 

        public GameResults(int totalNumberOfPlayers)
        {
            _sequenceOfElimination = new int[totalNumberOfPlayers - 1];
        }

        public void AddEliminated(int eliminatedPlayer)
        {
            if (_eliminatedIndex >= _sequenceOfElimination.Length)
            {
                throw new Exception(
                    string.Format(
                        "Unable to add eliminated player {0}. There are already {1} players in the elimination list",
                        eliminatedPlayer, _sequenceOfElimination.Length));
            }

            _sequenceOfElimination[_eliminatedIndex++] = eliminatedPlayer;
        }

        public int WinningPlayer { get; set; }

        public IEnumerable<int> Eliminated
        {
            get { return _sequenceOfElimination; }
        }
    }

}