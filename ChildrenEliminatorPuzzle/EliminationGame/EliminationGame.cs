﻿using System;

namespace ChildrenEliminatorPuzzle
{
    public interface IEliminationGame
    {
        /// <summary>
        ///  perform elminiation game using specifried parameters and return result of the game
        /// </summary>
        /// <param name="gameParameters">parameters of the game</param>
        /// <returns>result of the game - winning player and sequence of elminiated players</returns>
        IGameResults Run(GameParameters gameParameters);
    }

    /// <summary>
    /// Game engine - applies game rules using passed parameters and returns game result
    /// Relies on specific Circular storage
    /// </summary>
    public class EliminationGame : IEliminationGame
    {
        public EliminationGame(ICircularStorage circularStorage)
        {
            CircularStorage = circularStorage;
        }

        private ICircularStorage CircularStorage { get; set; }

        public IGameResults Run(GameParameters gameParameters)
        {
            VerifyGameParameters(gameParameters);

            var gameResult = new GameResults(gameParameters.NumberOfPlayers);

            var moveForwardCount = gameParameters.EliminationCount - 1;

            // allocaion should be done in contructor, however that will require a factory (or DI)
            CircularStorage.Allocate(gameParameters.NumberOfPlayers);

            while (CircularStorage.Count > 1)
            {
                CircularStorage.Move(moveForwardCount);

                gameResult.AddEliminated(CircularStorage.RemoveCurrent());              
            }

            gameResult.WinningPlayer = CircularStorage.Current;
         
            return gameResult;
        }

        static void VerifyGameParameters(GameParameters gameParameters)
        {
            if (gameParameters == null)
            {
                throw new ArgumentNullException("gameParameters", "Game parameters is NULL");
            }
            if (gameParameters.NumberOfPlayers <= 0)
            {
                ThrowException("Number of players should be positive", gameParameters);
            }
            if (gameParameters.EliminationCount <= 0)
            {
                ThrowException("Elimination Cycle should be positive", gameParameters);
            }
        }

        static void ThrowException(string errorMessage, GameParameters gameParameters)
        {
               throw new Exception(String.Format("{0}. Number of players:{1} elimination cycle:{2}",
                     errorMessage,gameParameters.NumberOfPlayers, gameParameters.EliminationCount));
            
        }
      
    }
}